## DevOps Process Demo

This docker-compose container set allows one to demonstrate the critical features and components of a traditional DevOps process.

### Prerequisites:

To use this on a Windows host, make sure that this setting in Docker Desktop is enabled:
"Expose daemon on tcp://localhost:2375 without TLS"

### Docker compose setup

On OSX, once you've cloned the repo do:
```
cd devops-demo
docker-compose up -d
```

On Windows do:
```
cd devops-demo
docker-compose -f docker-compose-win.yml up -d
```

Wait a few seconds for the jenkins container to come up.

### Bitbucket clone of node-web-app project

To create your own copy of the node-web-app project:

Import the "node-web-app" repo from this existing repo: https://bitbucket.org/atrent_discovery/node-web-app/src/master/

Use the bitbucket import tool.

Click the '+' in the sidebar on the left in bitbucket.

Under CREATE, click 'Repository'

Look for the 'Import Repository' link on the top right and click it or just navigate to: https://bitbucket.org/repo/import

For the URL, paste in: https://atrent_discovery@bitbucket.org/atrent_discovery/node-web-app.git

Leave 'Requires Authorization' unchecked.

The owner should be your user.

For 'Repository name' enter: node-web-app

Uncheck 'This is a private repository'

Click 'Import repository'

After waiting a few seconds for the import to complete, you should now have your own copy of the node-web-app repo in your bitbucket account.

### Jenkins/Docker build job setup

The custom jenkins image that was built from the Dockerfile in this repo has the docker cli client baked into the image.

Note: This jenkins setup skips using any plugins to maintain strict simplicity.

Navigate to: http://localhost:8080/view/all/newJob

Enter a name like "node-web-app"

Click "Freestyle project"

Click "OK"

Scroll down to the "Build" section

Click "Add build step > Execute shell"

In the box that appears in "Execute shell > Command" insert the following code:
```
rm -rf $(ls -a | tail -n +3) #quick hack to clean up the folder on each job run, due to no workspace cleanup plugin installed yet
#change the line below to your repo!
git clone https://atrent_discovery@bitbucket.org/atrent_discovery/node-web-app.git
#export DOCKER_HOST=tcp://docker.for.win.localhost:2375 #on windows uncomment this line
cd $WORKSPACE/node-web-app
docker build -t node-web-app:${BUILD_ID} .
docker tag node-web-app:${BUILD_ID} node-web-app:latest
```

Scroll down and click "Save"

When the job page reloads, click "Build Now" in the left hand column

Check the job log to see the image was built

Verify the image was built on your system using:
```
docker images | grep node-web-app
```

### Docker test

Run the image:
```
docker run -p 49160:8080 -d node-web-app
```

Verify its running with:
```
curl -i localhost:49160
```

Or by navigating to http://localhost:49160 in your web browser

You should see "Hello world" displayed on the page or in the curl output

Stop and remove the container with:
```
docker stop $(docker ps -a | grep node-web-app | awk '{ print $1 }') && docker rm $(docker ps -a | grep node-web-app | awk '{ print $1 }')
```

###

To demonstrate the quick iteration capablities of the combination of Jenkins and code in the gitlab repo, just edit the line 12 of the node-web-app server.js file directly in your copy of the node-web-app repo. Change ('Hello world\n') to ('Hello new world\n'), commit the change, and then run the Jenkins build job again. After the change if you run the new node-web-app container, you will see the changes in the curl output.

#### Cleanup
To remove all docker images created by this:
```
docker image rm --force $(docker images | grep node-web-app)
```

#### TODO:
Add how-to for running node watch to do live dev tests

Add git plugin to jenkins and build on commit watch to jenkins job

Add sonarqube or other code quality test instrumentation to buid process

Add selenium testing

Add security testing

Add deployment scenario
